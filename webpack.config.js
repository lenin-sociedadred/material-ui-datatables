const path = require('path');

module.exports = {
	context: path.resolve(__dirname, 'src'),
	entry: {
		app: './app.js',
	},
	output: {
		filename: './bundle.js',
	},
	module: {
			loaders: [
				// JSX and JS transpilation using babel
				{
					test: /\.js|\.jsx$/,
					exclude: /node_modules/,
					loaders: ['babel-loader']//, 'eslint-loader'
				}
			]
	},
	devServer: {
			contentBase: path.join(__dirname, "src"),
			compress: false,
			host: "0.0.0.0",
			port: 3335,
			// Display only errors to reduce the amount of output.
			stats: 'errors-only'
	}
};
